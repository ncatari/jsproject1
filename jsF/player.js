/**
 * Created by nelsoncatari on 30/07/2014.
 */
PlayersGame = function (name,nickname) {
    this.namePlayer = name;
    this.nicknamePlayer = nickname;
    this.field = null;

}

PlayersGame.prototype.createField = function(){
    this.field = new Field();
};