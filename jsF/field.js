/**
 * Created by nelsoncatari on 30/07/2014.
 */

    //  ' _ ' this mean, private methods
Field = function(){

    this.size = SIZE;
    this.status = INITIAL_STATUS;
    var _field = new Array();
    this.ships = new Array();
    var count = 0;
    var pX = 0;
    var pY = 0;
    var _vXm = new Array();


    // This is a getter method
    this.getField = function(){
        return _field;
    };

    // This is a setter method
    this.setField = function (field){
        _field = field;
    };

    this.getCoordinate = function (){
        return pX;
    }

    this.getVector = function (){
        return _vXm;
    }


    this.setVector = function (vX){
        _vXm = vX;

    }

    // Adding ship in the field with coordinate.

    this.addShipToField = function(ship) {


        pX = ship.getPosition();
        pY = ship.getPosition();
        pX = pX[0];
        pY = pY[1];


        if (pY == SIZE-1){

            pY = pY - 2;

        }

        if (pY == SIZE-2){

            pY = pY - 1;

        }

        console.log("This is PX:"+pX);
        console.log("This is PY:"+pY);

        //Assign ID in a register of ships.


        var getID = ship.getId();
        this.ships[count] = getID;// Save ID´s ship.
        count = count+1;
        console.log("Get ID drawing:"+getID);

        for(var j=0; j < MAX_SHIP_SIZE; j++){
            _field [pX][pY] = getID;
            pY = pY + 1;
        }

        //console.log(_field);

    };

    // call to methods after the getter and setter always.
    this.createField();
    this.createShips();
    this.draw();

};

Field.prototype.createField = function () {

// Create matrix

    var m = [];

    for (var a = 0 ; a < SIZE; a++) {
        m [a] =  new Array (SIZE);
    }

// Create Field

    for (var i=0; i < SIZE; i++) {
        for (var j=0; j < SIZE; j++){
            m [i][j] = 'X';

        }
    }

    this.setField(m);

};

Field.prototype.draw = function() {

    console.log(this.getField().join('\n'));

};


Field.prototype.createShips = function() {

    var vX= new Array();
    var count = 0;
    var j=0;
    var i=0;
    var newShip;

    // 1 2 1
    while( i < NUMBERS_OF_SHIPS) {

        newShip =new Ship(i);
        console.log("Processing ....:"+i);
        j=0;

        if (i == 0){


            this.addShipToField(newShip);

            vX[i]=this.getCoordinate();

            console.log("Printing vX:"+vX[i]);


        }

        else{

            console.log("Entering.............");
            count = vX.length;

            while(j < count){

                var postX = newShip.getPosition();
                postX = postX[0];



                if(postX != vX[j]) {

                    j=j+1;

                }
                else{
                    newShip =new Ship(i);
                }


            }




        }
        i = i+1;
        vX[i]=postX;
        this.addShipToField(newShip);
        this.setVector(vX);
        console.log("Vector:"+vX);

    }

};

