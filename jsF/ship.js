/**
 * Created by nelsoncatari on 30/07/2014.
 */
var Ship = function(id){


    var _id = id;

    var _position = new Array();
    this.sizeX = null;
    this.sizeY = null;
    this.status = ALIVE;
    this.direction = LANDSCAPE;

    this.getId = function(){
        return _id;

    }

    this.getPosition = function(){
        return _position;
    }

    this.setPosition = function(positionX,positionY){


        _position[0] = positionX;
        _position[1] = positionY;

        //console.log("Testing posX - Class ship:"+_position[0]);
        //console.log("Testing posY - Class ship:"+_position[1]);
    }


    this.create();

};

Ship.prototype.create = function(){

    //var i=0;

    var shipSizeX = parseInt(Math.random() * MAX_SHIP_SIZE);
    var shipSizeY = parseInt(Math.random() * MAX_SHIP_SIZE);

    if(shipSizeX == 0) shipSizeX = 1;
    if(shipSizeY == 0) shipSizeY = 1;



    this.sizeX = parseInt(Math.random() * SIZE - shipSizeX);
    this.sizeY = parseInt(Math.random() * SIZE - shipSizeY);

    if (this.sizeX < 0){
        this.sizeX = -(this.sizeX);
    }

    if (this.sizeY < 0){
        this.sizeY = -(this.sizeY);
    }




    this.setPosition(this.sizeX,this.sizeY);



};

