/**
 * Created by nelsoncatari on 17/07/2014.
 */

var createGame2 = function(size){
    
	var field = new Array(size);
    var shipmaxsize = 4;

    //create game field
    
	for (a = 0; a < size; a++){
	
	field[a]=new Array(size);
	
	};

    //fill game field
    for (var i = 0; i<size; i++){
        for(var j = 0; j< size; j++){
            field[i][j] = "X";
        }
    }
    //createship in any direction
    var createShip = function(id) {
        //var shipSize = parseInt(Math.random() * shipmaxsize);
        var shipSize = 2;
        var posx= parseInt(Math.random() * size - shipSize);
        var posy = parseInt(Math.random() * size - shipSize);
        var dir = parseInt(Math.random() * 2);
        
		if (shipSize == 0) shipSize = 1;
        if (dir == 0 ){
            for (var i = 0; i < shipSize; i++){
                field[posx + i][posy] = id;
            }

        }
        else{
            for (var j = 0; j < shipSize; j++){
                field[posx][posy + j] = id;
            }
        };


        return shipSize;
    };

    var ship = createShip(1);
    //evaluate shot
    var evaluation = function(shotx,shoty, shipId){
        if (field[shotx][shoty] == 1) {
            ship--;
            if (ship == 0)
                console.log("YOU KILL THE SHIP");
            else
                console.log("HIT THE SHIP");
        }
        else
            console.log("FAIL");
    };

    while (ship != 0) {
        shotx = window.prompt("Enter Shotx?");
        shoty = window.prompt("Enter Shoty?");
        evaluation(shotx,shoty, 1);
        field[shotx][shoty] = "O";

        for(c = 0; c<size; c++){
            console.log(field[c].join(""));
            console.log(" ");
        }
    }

}